using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MemorySequence
{
    public class GameManager : MonoBehaviour
    {
        [Header("Script Manager")]
        [SerializeField] private StartMenuController startMenu;
        [SerializeField] private PausePopUpController pausePopUp;
        [SerializeField] private GameOverPopUpController gameOverPopUp;
        [SerializeField] private Sequencer sequencer;
        [SerializeField] private PlayerController playerController;

        private bool isPaused;
        private bool isHard = false;
        private GameState currentState = GameState.None;

        //Set number of health and sequence number
        private const int maxHealth = 3;
        private int currentHealth = 0;
        private int[] currentSequence;
        private int currentSequenceLength;

        //Set sequence minimum and maximum number
        private const int minSequenceLength = 2;
        private const int maxSequenceLength = 3;
        private const float maxSequenceTime = 3f;

        private List<int> inputSequence = new List<int>();

        public event Action<bool> SequencePressed;
        public event Action SequenceFinished;
        public event Action<bool> GameOver;

        private void Awake()
        {
            startMenu.QuitPressed += OnQuitPressed;
            startMenu.NormalPressed += OnNormalPressed;
            startMenu.HardPressed += OnHardPressed;

            pausePopUp.ContinuePressed += OnContinuePressed;
            pausePopUp.ResetPressed += OnResetPressed;
            pausePopUp.QuitPressed += OnQuitPressed;

            gameOverPopUp.PlayPressed += OnResetPressed;
            gameOverPopUp.QuitPressed += OnQuitPressed;

            sequencer.ButtonPressed += OnSequencerPressed;
        }

        private void OnDestroy()
        {
            startMenu.QuitPressed -= OnQuitPressed;
            startMenu.NormalPressed -= OnNormalPressed;
            startMenu.HardPressed -= OnHardPressed;

            pausePopUp.ContinuePressed -= OnContinuePressed;
            pausePopUp.ResetPressed -= OnResetPressed;
            pausePopUp.QuitPressed -= OnQuitPressed;

            gameOverPopUp.PlayPressed -= OnResetPressed;
            gameOverPopUp.QuitPressed -= OnQuitPressed;

            sequencer.ButtonPressed -= OnSequencerPressed;
        }

        private void Update()
        {
            if (currentState != GameState.PlayerInput) return;

            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                Pause();
            }
        }

        private void StartGame()
        {
            currentHealth = maxHealth;
            playerController.SetHealth(currentHealth);
            currentSequenceLength = minSequenceLength - 1;
            RandomizeSequence();

            gameOverPopUp.Hide();
            playerController.Show();
            sequencer.Show();
            sequencer.Activate();

            ShowNextSequence();
        }

        private void ShowNextSequence()
        {
            currentSequenceLength++;
            if (isHard)
            {
                RandomizeSequence();
            }

            StartCoroutine(DoSequence());
        }

        private IEnumerator DoSequence() // int[] sequence, int count
        {
            SetGameState(GameState.Sequencing);

            yield return sequencer.ShowSequence(currentSequence, currentSequenceLength, maxSequenceTime);
            sequencer.Activate();

            SetGameState(GameState.PlayerInput);
        }

        private void RandomizeSequence()
        {
            currentSequence = new int[maxSequenceLength];

            for (int i = 0; i < maxSequenceLength; i++)
            {
                currentSequence[i] = Random.Range(0, 4);
            }
        }

        private void SetGameState(GameState state)
        {
            currentState = state;
            if (currentState == GameState.PlayerInput)
            {
                inputSequence.Clear();
                sequencer.Activate();
            }
            else
            {
                sequencer.Deactivate();
            }
        }

        private void Pause()
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                pausePopUp.Show();
            }

            else
            {
                pausePopUp.Hide();
            }
        }

        private void OnGameOver(bool isWinGame)
        {
            GameOver?.Invoke(isWinGame);
            gameOverPopUp.Show();
            playerController.Hide();
        }

        #region Button Event
        private void OnQuitPressed()
        {
            Application.Quit();
        }

        private void OnNormalPressed()
        {
            isHard = false;
            StartGame();
        }

        private void OnHardPressed()
        {
            isHard = true;
            StartGame();
        }

        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnResetPressed()
        {
            StartGame();
            SetGameState(GameState.Sequencing);

            gameOverPopUp.Hide();
            playerController.Reset();
            playerController.Show();
        }

        private void OnSequencerPressed(ButtonType buttonType)
        {
            if (currentState != GameState.PlayerInput) return;

            if ((int)buttonType != currentSequence[inputSequence.Count])
            {
                currentHealth--;
                if (currentHealth <= 0)
                {
                    SetGameState(GameState.Calculating);
                    OnGameOver(false);
                    return;
                }

                playerController.SetHealth(currentHealth);
                playerController.ShowDialogue(false);
                inputSequence.Clear();
                SequencePressed?.Invoke(false);

                StartCoroutine(DoSequence());
                return;
            }

            else
            {
                inputSequence.Add((int)buttonType);
                playerController.ShowDialogue(true);
                SequencePressed?.Invoke(false);
            }

            if (inputSequence.Count == maxSequenceLength)
            {
                SetGameState(GameState.Calculating);
                OnGameOver(true);
                return;
            }

            if (inputSequence.Count == currentSequenceLength)
            {
                SequenceFinished?.Invoke();
                ShowNextSequence();
            }
        }
        #endregion
    }
}


