using System.Collections;
//using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MemorySequence.Input
{
    [RequireComponent(typeof(Button))]
    public class ButtonView : MonoBehaviour
    {
        [SerializeField] private Button button;
        //[SerializeField] private TextMeshProUGUI text;

        [SerializeField] private Image image;
        [SerializeField] private Sprite normalSprite;
        [SerializeField] private Sprite pressedSprite;
        [SerializeField] private Sprite disabledSprite;
        
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color pressedColor = Color.black;
        
        private void Awake()
        {
            button.StateChanged += OnStateChanged;
        }

        private void OnStateChanged(bool active, bool pressed)
        {
            image.sprite = active ? pressed ? pressedSprite : normalSprite : disabledSprite == null ? normalSprite : disabledSprite;
            //text.color = active ? pressed ? pressedColor : normalColor : normalColor;
        }

        public IEnumerator SimulatePress(float delay)
        {
            image.sprite = pressedSprite;
            //text.color = pressedColor;
            yield return new WaitForSeconds(delay);
            image.sprite = normalSprite;
            //text.color = normalColor;
        }
    }
}