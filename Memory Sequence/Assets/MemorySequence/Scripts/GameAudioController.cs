using UnityEngine;

namespace MemorySequence
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioSource audioSource;

        [SerializeField] private AudioClip pressRight;
        [SerializeField] private AudioClip pressWrong;
        [SerializeField] private AudioClip sequenceRight;
        [SerializeField] private AudioClip gameWin;
        [SerializeField] private AudioClip gameLose;

        private void Awake()
        {
            gameManager.SequencePressed += OnSequencePressed;
            gameManager.SequenceFinished += OnSequenceFinished;
            gameManager.GameOver += OnGameOver;
        }

        // Start is called before the first frame update
        private void OnSequencePressed(bool isPressed)
        {
            audioSource.PlayOneShot(pressRight ? pressRight : pressWrong);
        }

        private void OnSequenceFinished()
        {
            audioSource.PlayOneShot(sequenceRight);
        }

        private void OnGameOver(bool isWin)
        {
            audioSource.PlayOneShot(isWin ? gameWin : gameLose);
        }
    }
}


