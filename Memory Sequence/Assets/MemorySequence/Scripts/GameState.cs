namespace MemorySequence
{
	public enum GameState
	{
		None,
		Sequencing,
		PlayerInput,
		Calculating,
		GameOver
	}
}