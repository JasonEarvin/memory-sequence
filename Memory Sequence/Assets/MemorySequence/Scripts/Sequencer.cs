using System;
using System.Collections;
using MemorySequence.Input;
using UnityEngine;


namespace MemorySequence
{
    public class Sequencer : MenuController
    {
        [SerializeField] private SequenceButton[] sequenceButtons;
        private ButtonView[] sequenceButtonViews;

        private bool isActive = false;

        public event Action<ButtonType> ButtonPressed;

        private int nextButtonIndex;

        private void Awake() 
        {
            sequenceButtonViews = new ButtonView[sequenceButtons.Length];

            for(int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].SequenceButtonUp += OnSequenceDone;
                sequenceButtonViews[i] = sequenceButtons[i].GetComponent<ButtonView>();
            }
        }

        private void OnDestroy() 
        {
            for(int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].SequenceButtonUp -= OnSequenceDone;
            }
        }

        public IEnumerator ShowSequence(int[] currentSequence, int currentSequenceLength, float maxSequenceTime)
        {
            for(int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].DisableInput();
            }

            yield return new WaitForSeconds(1f);
            float delay = maxSequenceTime / currentSequenceLength;
            float buttonDelay = delay - 0.2f;
            YieldInstruction yieldInstruction = new WaitForSeconds(delay);
            for(int i = 0; i < currentSequenceLength; i++)
            {
                StartCoroutine(sequenceButtonViews[currentSequence[i]].SimulatePress(buttonDelay));
                yield return yieldInstruction;
            }

            for(int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].EnableInput();
            }
        }

        // Start is called before the first frame update
        private void OnSequenceDone(ButtonType buttonType)
        {

            if (!isActive) return;

            ButtonPressed?.Invoke(buttonType);
        }

        public void Activate()
        {
            isActive = true;
        }
        public void Deactivate()
        {
            isActive = false;
        }
    }

}
