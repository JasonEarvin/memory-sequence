using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MemorySequence
{
    public class PlayerController : MenuController
    {
        // Start is called before the first frame update
        [SerializeField] private Image mascot;

        [SerializeField] private GameObject dialogueObject;
        [SerializeField] private Text dialogueText;
        [SerializeField] private string[] correct = new[] {"", "", ""};
        [SerializeField] private string[] wrong = new[] {"", "", ""};
        [SerializeField] private Sprite[] correctMascot;
        [SerializeField] private Sprite[] wrongMascot;
        [SerializeField] private Sprite defaultMascot;

        [SerializeField] private Image[] healthCheck;

        public void ShowDialogue(bool isSHow)
        {
            CancelInvoke(nameof(HideDialogue));
            string dialogue = "";

            if (isSHow)
            {
                mascot.sprite = correctMascot[Random.Range(0, correctMascot.Length)];
                dialogue = correct[Random.Range(0, correct.Length)];
            }

            else
            {
                mascot.sprite = wrongMascot[Random.Range(0, wrongMascot.Length)];
                dialogue = wrong[Random.Range(0, wrong.Length)];
            }

            dialogueText.text = dialogue;
            dialogueObject.SetActive(true);
            canvas.enabled = true;

            Invoke(nameof(HideDialogue), 0.5f);
        }

        private void HideDialogue()
        {
            dialogueObject.SetActive(false);
        }

        public void SetHealth(int health)
        {
            for(int i = 0; i < healthCheck.Length; i++)
            {
                healthCheck[i].enabled = i < health;
            }
        }

        public void Show()
        {
            canvas.enabled = true;
        }

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void Reset()
        {
            mascot.sprite = defaultMascot;
            HideDialogue();
        }
    }

}

