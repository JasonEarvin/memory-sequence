using System;
using MemorySequence.Input;
using UnityEngine;

namespace MemorySequence
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private Button normalButton;
        [SerializeField] private Button hardButton;

        private int currentState = 0;

        public event Action PlayPressed;
        public event Action QuitPressed;

        public event Action NormalPressed;
        public event Action HardPressed;

        private void Update()
        {
            if (currentState == 1)
            {
                if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
                {
                    SwitchState(0);
                }
            }
        }

        private void Start()
        {
            playButton.ButtonUp += OnPlayPressed;
            quitButton.ButtonUp += OnQuitPressed;

            normalButton.ButtonUp += OnNormalPressed;
            hardButton.ButtonUp += OnHardPressed;
        }

        private void OnDestroy()
        {
            playButton.ButtonUp -= OnPlayPressed;
            quitButton.ButtonUp -= OnQuitPressed;

            normalButton.ButtonUp -= OnNormalPressed;
            hardButton.ButtonUp -= OnHardPressed;
        }

        #region Button Event
        private void OnPlayPressed()
        {
            SwitchState(1);
            PlayPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            QuitPressed?.Invoke();
        }

        private void OnNormalPressed()
        {
            Hide();
            NormalPressed?.Invoke();
        }

        private void OnHardPressed()
        {
            Hide();
            HardPressed?.Invoke();
        }
        #endregion

        private void SwitchState(int state)
        {
            currentState = state;

            if (state == 0)
            {
                playButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(true);

                normalButton.gameObject.SetActive(false);
                hardButton.gameObject.SetActive(false);
            }

            else
            {
                playButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(false);

                normalButton.gameObject.SetActive(true);
                hardButton.gameObject.SetActive(true);
            }
        }
    }
}


