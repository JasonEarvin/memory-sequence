using System;
using MemorySequence.Input;
using UnityEngine;

namespace MemorySequence
{
    public class GameOverPopUpController : MenuController
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button quitButton;

        public event Action PlayPressed;
        public event Action QuitPressed;

        private void Start()
        {
            playButton.ButtonUp += OnPlayPressed;
            quitButton.ButtonUp += OnQuitPressed;
        }

        private void OnDestroy()
        {
            playButton.ButtonUp -= OnPlayPressed;
            quitButton.ButtonUp -= OnQuitPressed;
        }

        #region Button Event
        private void OnPlayPressed()
        {
            PlayPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            QuitPressed?.Invoke();
        }
        #endregion
    }
}


