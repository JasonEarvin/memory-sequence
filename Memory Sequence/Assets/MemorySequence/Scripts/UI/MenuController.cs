using UnityEngine;

namespace MemorySequence
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField] protected Canvas canvas;

        public virtual void Show()
        {
            canvas.enabled = true;
        }

        public virtual void Hide()
        {
            canvas.enabled = false;
        }
    }
}
