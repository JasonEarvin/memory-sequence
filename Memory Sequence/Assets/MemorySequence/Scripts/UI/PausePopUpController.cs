using System;
using MemorySequence.Input;
using UnityEngine;

namespace MemorySequence
{
    public class PausePopUpController : MenuController
    {
        [SerializeField] private Button continueButton;
		[SerializeField] private Button resetButton;
		[SerializeField] private Button quitButton;

		public event Action ContinuePressed;
		public event Action ResetPressed;
		public event Action QuitPressed;
		
		[SerializeField] private AudioSource _audioSource;

		private void Start()
		{
			continueButton.ButtonUp += OnContinuePressed;
			resetButton.ButtonUp += OnResetPressed;
			quitButton.ButtonUp += OnQuitPressed;
		}

		private void OnDestroy()
		{
			continueButton.ButtonUp -= OnContinuePressed;
			resetButton.ButtonUp -= OnResetPressed;
			quitButton.ButtonUp -= OnQuitPressed;
		}

		private void OnContinuePressed()
		{
			Hide();
			ContinuePressed?.Invoke();
		}

		private void OnResetPressed()
		{
			Hide();
			ResetPressed?.Invoke();
		}

		private void OnQuitPressed()
		{
			Hide();
			QuitPressed?.Invoke();
		}

		public override void Show()
		{
			base.Show();
			_audioSource.Play();
		}
    }
}


