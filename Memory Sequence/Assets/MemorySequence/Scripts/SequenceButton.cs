using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MemorySequence.Input
{
    public class SequenceButton : Button, IPointerUpHandler
    {
        [SerializeField] private ButtonType buttonType;

        public event Action<ButtonType> SequenceButtonUp;

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);

            SequenceButtonUp?.Invoke(buttonType);
        }
    }
}

